[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

function ContinueBtn-Action() {
    # Make sure the cell, provider and dir values are not empty
    $errorMsg = Validate-Values

    # Create the number.txt with the number and cell provider
    if ($errorMsg -match "-") {
        # MessageDialog saying that not all values have been entered
        [System.Windows.Forms.MessageBox]::Show($errorMsg, "Invalid Input")
        return;
    } else {
        Move-Result
        Move-Files
        Create-Shortcuts
        Create-SchedTask
        Run-Program
    }

}

function Validate-Values() {
    $errorMsg = "The following fields are invalid:`r`n`r`n";
    $errCnt = 1;
    $cell = ($phoneTextBox.Text).Replace("(", ""). Replace(")", "").Replace("-", "").Replace(" ", "");
    if ([string]::IsNullOrEmpty($cell) -or $cell.Length -ne 10) {
        $phoneTextBox.Text = "";
        $errorMsg += "   -  Phone Number`r`n";
        $errCnt += 1;
    } else {
        $phoneTextBox.Text = $cell;
    }

    $provider = $providerTextBox.Text;
    if ([string]::IsNullOrEmpty($provider)) {
        $providerTextBox.Text = "";
        $errorMsg += "   -  Phone Provider`r`n";
        $errCnt += 1;
    }

    $dir = $installDirTextBox.Text;
    if ([string]::IsNullOrEmpty($dir)) {
        $installDirTextBox.Text = "";
        $errorMsg += "   -  Install Directory`r`n";
        $errCnt += 1;
    }

    return $errorMsg;
}

function Move-Result() {
    $text = "$($phoneTextBox.Text)`r`n$($providerTextBox.Text)"
    Set-Content -Path "$PSScriptRoot\bin\result.txt" -Value $text
}

function Move-Files() {
    $installDir = $installDirTextBox.Text
    $fromPath = $($PSScriptRoot) + "\*"
    $toPath = $($installDir) + "KSL-Watcher\*"
    
    if (!(Test-Path $installDir)) {
        md "$($installDir)\KSL-Watcher\"
    }  

    if(!(Test-Path "$($installDir)\bin")) {
        md "$($installDir)\KSL-Watcher\bin\"
    }

    Move-FilesRecursively
}

function Move-FilesRecursively() {
    Copy-Item -Path "$($PSScriptRoot)\*" "$($installDir)\KSL-Watcher\" -ErrorAction SilentlyContinue | Out-Null
    Copy-Item -Path "$($PSScriptRoot)\bin\*" "$($installDir)\KSL-Watcher\bin\" -ErrorAction SilentlyContinue | Out-Null
}

function Create-Shortcuts() {
    $installDir = $installDirTextBox.Text;

    if ($desktopCheckBox.Checked) {
        $dTargetFile = "$installDir\KSL-Watcher\bin\KSL-Watcher.exe"
        $dShortcutFile = "C:\Users\$env:username\Desktop\KSL-Watcher.lnk"
        $dWScriptShell = New-Object -ComObject WScript.Shell
        $dShortcut = $dWScriptShell.CreateShortcut($dShortcutFile)
        $dShortcut.TargetPath = $dTargetFile
        $dShortcut.WorkingDirectory = "$installDir\KSL-Watcher\bin"
        $dShortcut.Save()
    }

    if ($startMenuCheckBox.Checked) {
        $TargetFile = "$installDir\KSL-Watcher\bin\KSL-Watcher.exe"
        $ShortcutFile = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\KSL-Watcher.lnk"
        $WScriptShell = New-Object -ComObject WScript.Shell
        $Shortcut = $WScriptShell.CreateShortcut($ShortcutFile)
        $Shortcut.TargetPath = $TargetFile
        $Shortcut.WorkingDirectory = "$installDir\KSL-Watcher\bin"
        $Shortcut.Save()
    }
}

function Create-SchedTask() {
    $installDir = $installDirTextBox.Text;
    $targetExe = "$installDir\KSL-Watcher\bin\KSL-Watcher.exe"

    $action = New-ScheduledTaskAction -Execute $targetExe -Argument '-NoProfile -WindowStyle Hidden -command "cd $installDir\bin; Start-Process .\KSL-Watcher.exe"'
    $trigger =  New-ScheduledTaskTrigger -Once -At 7am -RepetitionDuration (New-TimeSpan -Days 3000) -RepetitionInterval  (New-TimeSpan -Minutes 15)
    $settings = New-ScheduledTaskSettingsSet -Hidden -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -StartWhenAvailable -RunOnlyIfNetworkAvailable
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "KSL-Watcher" -Description "Runs every 15 minutes." -Settings $settings
}

function Run-Program() {

}

function Get-InstallDir() {
    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
    $foldername.rootfolder = "MyComputer";

    if($foldername.ShowDialog() -eq "OK")
    {
        $folder += $foldername.SelectedPath;
    }

    $installDirTextBox.Text = $folder;
}

function Generate-Form {
    $Form = New-Object System.Windows.Forms.Form 
    $Form.Text = "KSL Watcher Setup"
    $Form.Size = New-Object System.Drawing.Size(600,400) 
    $Form.StartPosition = "CenterScreen"
    $Icon = [system.drawing.icon]::ExtractAssociatedIcon("bin\KSL-Watcher.exe")
    $Form.Icon = $Icon
    $Image = [system.drawing.image]::FromFile("KSL-Logo.png")
    $Form.BackgroundImage = $Image

    $Form.KeyPreview = $True
    $Form.Add_KeyDown({if ($_.KeyCode -eq "Enter") 
        {$x=$phoneTextBox.Text;$Form.Close()}})
    $Form.Add_KeyDown({if ($_.KeyCode -eq "Escape") 
        {$Form.Close()}})

    $title = New-Object System.Windows.Forms.Label
    $title.Location = New-Object System.Drawing.Size(10,20)
    $title.Size = New-Object System.Drawing.Size(75,23)
    $title.Text = "KSL Setup Wizard"
    $title.Font = New-Object System.Drawing.Font("Times New Roman",24,[System.Drawing.FontStyle]::Italic)
    $title.BackColor = "Transparent"
    $title.AutoSize = $True
    $Form.Controls.Add($title)

    $OKButton = New-Object System.Windows.Forms.Button
    $OKButton.Location = New-Object System.Drawing.Size(480,150)
    $OKButton.Size = New-Object System.Drawing.Size(75,23)
    $OKButton.Text = "Continue"
    $OKButton.Add_Click({ContinueBtn-Action})
    $Form.Controls.Add($OKButton)

    $CancelButton = New-Object System.Windows.Forms.Button
    $CancelButton.Location = New-Object System.Drawing.Size(400,150)
    $CancelButton.Size = New-Object System.Drawing.Size(75,23)
    $CancelButton.Text = "Cancel"
    $CancelButton.Add_Click({$Form.Close()})
    $Form.Controls.Add($CancelButton)

    $phoneLabel = New-Object System.Windows.Forms.Label
    $phoneLabel.Location = New-Object System.Drawing.Size(10,83) 
    $phoneLabel.Size = New-Object System.Drawing.Size(90,20) 
    $phoneLabel.Text = "Phone Number:"
    $Form.Controls.Add($phoneLabel) 

    $phoneTextBox = New-Object System.Windows.Forms.TextBox 
    $phoneTextBox.Location = New-Object System.Drawing.Size(100,80) 
    $phoneTextBox.Size = New-Object System.Drawing.Size(100,20) 
    $Form.Controls.Add($phoneTextBox) 

    $providerLabel = New-Object System.Windows.Forms.Label
    $providerLabel.Location = New-Object System.Drawing.Size(10,113) 
    $providerLabel.Size = New-Object System.Drawing.Size(90,20) 
    $providerLabel.Text = "Cell Provider:"
    $Form.Controls.Add($providerLabel) 

    $providerTextBox = New-Object System.Windows.Forms.TextBox 
    $providerTextBox.Location = New-Object System.Drawing.Size(100,110) 
    $providerTextBox.Size = New-Object System.Drawing.Size(100,20) 
    $Form.Controls.Add($providerTextBox)

    $installDirLabel = New-Object System.Windows.Forms.Label
    $installDirLabel.Location = New-Object System.Drawing.Size(230,83) 
    $installDirLabel.Size = New-Object System.Drawing.Size(90,20) 
    $installDirLabel.Text = "Install Directory:"
    $Form.Controls.Add($installDirLabel) 

    $installDirTextBox = New-Object System.Windows.Forms.TextBox 
    $installDirTextBox.Location = New-Object System.Drawing.Size(330,80) 
    $installDirTextBox.Size = New-Object System.Drawing.Size(200,20) 
    $Form.Controls.Add($installDirTextBox)

    $browseButton = New-Object System.Windows.Forms.Button
    $browseButton.Location = New-Object System.Drawing.Size(523,79)
    $browseButton.Size = New-Object System.Drawing.Size(30,20)
    $browseButton.Text = " ..."
    $browseButton.Add_Click({Get-InstallDir})
    $Form.Controls.Add($browseButton)

    $desktopCheckBox = New-Object System.Windows.Forms.CheckBox
    $desktopCheckBox.Location = New-Object System.Drawing.Size(12,150) 
    $desktopCheckBox.Size = New-Object System.Drawing.Size(15,15)
    $desktopCheckBox.Checked = $True
    $Form.Controls.Add($desktopCheckBox)

    $desktopLabel = New-Object System.Windows.Forms.Label
    $desktopLabel.Location = New-Object System.Drawing.Size(35,150) 
    $desktopLabel.Size = New-Object System.Drawing.Size(120,20) 
    $desktopLabel.Text = "Desktop Shortcut"
    $Form.Controls.Add($desktopLabel)

    $startMenuCheckBox = New-Object System.Windows.Forms.CheckBox
    $startMenuCheckBox.Location = New-Object System.Drawing.Size(12,175) 
    $startMenuCheckBox.Size = New-Object System.Drawing.Size(15,15)
    $startMenuCheckBox.Checked = $True
    $Form.Controls.Add($startMenuCheckBox)

    $startMenuLabel = New-Object System.Windows.Forms.Label
    $startMenuLabel.Location = New-Object System.Drawing.Size(35,175) 
    $startMenuLabel.Size = New-Object System.Drawing.Size(120,20) 
    $startMenuLabel.Text = "Start Menu Shortcut"
    $Form.Controls.Add($startMenuLabel)

    $Form.Topmost = $True

    $Form.Add_Shown({$Form.Activate()})
    [void] $Form.ShowDialog()
}

Generate-Form