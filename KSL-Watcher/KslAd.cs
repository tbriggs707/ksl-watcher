﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSL_Watcher
{
    public class KslAd
    {
        public Dictionary<string, string> Data { get; set; }
        
        public KslAd()
        {
            Data = new Dictionary<string, string>()
            {
                { "id",          "N/A" },
                { "url",         "N/A" },
                { "make",        "N/A" },
                { "model",       "N/A" },
                { "imgUrl",      "N/A" },
                { "title",       "N/A" },
                { "price",       "N/A" },
                { "mileage",     "N/A" },
                { "location",    "N/A" },
                { "description", "N/A" },
            };
        }    
    }
}
