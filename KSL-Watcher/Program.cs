﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace KSL_Watcher
{
    public class Program
    {
        static void Main(string[] args)
        {
            SMS sms = new SMS(GetNumber());
            DataStorage searchStore = new DataStorage();
            List<Search> savedSearches = searchStore.ReadFromFile("SavedSearches.xml");

            if (args.Length > 0 && args[0] == "-h")
                HeadlessMode(savedSearches, sms);
            else
                InteractiveMode(savedSearches, sms);

            searchStore.WriteResults("SavedSearches.xml", savedSearches);
        }

        #region Driver Methods
        private static void InteractiveMode(List<Search> savedSearches, SMS sms)
        {
            bool run = true;
            string input = string.Empty;

            while (run)
            {
                MainMenu();

                input = Console.ReadLine();
                string[] validInput = new[] { "1", "2", "3", "4", "q", "v", "a", "r", "c", "" };
                while (!validInput.Contains(input))
                {
                    Console.WriteLine("Invalid input, try again");
                    Console.Write("Input: ");
                    input = Console.ReadLine();
                }

                switch (input.ToLower())
                {
                    case "":
                    case "v":
                    case "1":
                        ViewAllSearches(savedSearches);
                        break;

                    case "a":
                    case "2":
                        AddNewSearch(savedSearches);
                        break;

                    case "r":
                    case "3":
                        RemoveSearch(savedSearches);
                        break;

                    case "c":
                    case "4":
                        HeadlessMode(savedSearches, sms);
                        break;

                    case "q":
                        return;
                }

                run = Run();
            }
        }

        private static void HeadlessMode(List<Search> savedSearches, SMS sms)
        {
            Console.Clear();
            Console.WriteLine("---Check for New Ads---\n");

            if (savedSearches.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("No searches have been saved.");
                Console.ResetColor();
                Thread.Sleep(2000);
            }
            else
            {
                foreach (var search in savedSearches)
                {
                    Console.WriteLine(search.Name + "...");
                    if (search.Url.Contains("auto"))
                        CarAction(search, savedSearches, sms);
                    else
                        GenericAction(search, savedSearches);
                }
            }
        }

        private static void CarAction(Search search, List<Search> savedSearches, SMS sms)
        {
            if (TimeToCheck(search))
            {
                Scraper scraper = new Scraper(search.Url);
                search.LastRunTime = DateTime.Now;

                // If the search hasn't been run yet, get all ad IDs so we have a baseline for next time
                if (search.KnownAdIds == null)
                {
                    GetKnownAdIds(search, scraper);
                    search.LastHit = DateTime.Now;
                    search.LastRunTime = DateTime.Now;
                    return;
                }

                List<KslAd> newAds = CompareAds(scraper.KslAds, search.KnownAdIds);
                int sentCount = 0;

                foreach (var ad in newAds)
                {
                    search.KnownAdIds.Add(ad.Data["id"]);

                    // Only send notifications for up to 5 ads per search
                    if (sentCount++ < 5)
                        SendNotification(search, ad, sms);
                }
            }
        }

        private static void GenericAction(Search search, List<Search> savedSearches)
        {
            return;
        }

        private static bool Run()
        {
            Console.Write("\nReturn to the Main Menu? [Y/N]: ");
            string input = Console.ReadLine().ToLower();

            while (!input.Contains("y") && !input.Contains("n") && input != "")
            {
                Console.WriteLine("Invalid input, try again");
                Console.Write("Return to the Main Menu? [Y/N]: ");
                input = Console.ReadLine().ToLower();
            }

            return input.Contains("y") || input == "" ? true : false;
        }

        private static void SendNotification(Search search, KslAd ad, SMS sms)
        {
            string make = ad.Data["make"];
            string model = ad.Data["model"];
            string price = ad.Data["price"];
            string location = ad.Data["location"];
            string mileage = ad.Data["mileage"];
            string url = ad.Data["url"];

            string msg = "New result for " + search.Name
                            + "\n     " + make + " " + model + " - " + price
                            + "\n     " + mileage + " miles in " + location
                            + "\n     " + url;

            sms.Send(msg);
            Console.WriteLine(msg);
            search.LastHit = DateTime.Now;
        }
        #endregion Driver Methods

        #region Helper Methods
        
        #region Menu/Output Methods
        private static void RemoveSearch(List<Search> savedSearches)
        {
            string name = string.Empty;
            Console.Clear();
            Console.WriteLine("---Remove Search---\n");

            if (savedSearches.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("No searches have been saved.");
                Console.ResetColor();
            }
            else
            {
                while (true)
                {
                    foreach (var search in savedSearches)
                        Console.WriteLine(search.Name);

                    Console.Write("\nName of Search to Remove: ");
                    name = Console.ReadLine();

                    foreach (var search in savedSearches)
                    {
                        if (search.Name.ToLower() == name.ToLower())
                        {
                            savedSearches.Remove(search);
                            Console.WriteLine("Success!");
                            return;
                        }
                    }

                    Console.WriteLine("\n" + name + " was not found, the options are:\n");
                }
            }
        }

        private static void AddNewSearch(List<Search> savedSearches)
        {
            string name = string.Empty;
            string url = string.Empty;
            string interval = string.Empty;
            bool correct = false;
            List<KslAd> knownAds = new List<KslAd>();

            Console.Clear();
            Console.WriteLine("---Add New Search---\n");
            while (!correct)
            {
                Console.Write("Name (Dodge Ram 2500): ");
                name = Console.ReadLine();
                ValidateName(name);

                Console.Write("Interval (every [15|0.5] [hours|days|weeks]): ");
                interval = Console.ReadLine();
                ValidateInterval(interval);

                Console.Write("Paste URL with 'Shift+Insert': ");
                url = Console.ReadLine();
                knownAds = ValidateUrl(savedSearches, url);

                Console.WriteLine("\nReview:");
                Console.WriteLine("  Name: " + name);
                Console.WriteLine("  Interval: " + interval);
                Console.WriteLine("  URL matches: " + knownAds.Count);

                Console.Write("\nConfirm? [Y/N]: ");
                string confirmAnswer = Console.ReadLine().ToLower();
                correct = string.IsNullOrEmpty(confirmAnswer) || confirmAnswer == "y";
            }

            savedSearches.Add(new Search()
            {
                Name = name,
                Url = url,
                Interval = interval,
                LastRunTime = DateTime.Now,
                LastHit = DateTime.Now,
                KnownAdIds = knownAds.Select(ad => ad.Data["id"]).ToList()
            });
        }

        private static void ViewAllSearches(List<Search> savedSearches)
        {
            Console.Clear();
            Console.WriteLine("---View All Saved Searches---\n");
            int cnt = 1;

            if (savedSearches.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("No searches have been saved.");
                Console.ResetColor();
            }
            else
            {
                foreach (var search in savedSearches)
                {
                    Console.WriteLine("[" + cnt++ + "] Name: " + search.Name);
                    Console.WriteLine("     Last Run Time: " + search.LastRunTime);
                    Console.WriteLine("     Last New Hit: " + search.LastHit);
                    Console.WriteLine("     Runs every: " + search.Interval);
                    Console.WriteLine();
                }
            }
        }

        private static void MainMenu()
        {
            Console.Clear();
            Console.WriteLine("---Main Menu---\n");
            Console.WriteLine("View All Searches   [V]");
            Console.WriteLine("Add New Search      [A]");
            Console.WriteLine("Remove Search       [R]");
            Console.WriteLine("Check All Searches  [C]");
            Console.WriteLine("Quit                [Q]");
            Console.Write("\nInput: ");
        }

        private static string AskForNewUrl(Search duplicateSearch)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            if (duplicateSearch == null)
            {
                Console.WriteLine("There was an issue with your URL, either the URL is invalid");
                Console.WriteLine("or the search returned no results.");
            }
            else
            {
                Console.WriteLine("The search URL is a duplicate of " + duplicateSearch.Name + ".");
            }
            Console.WriteLine("Please try another URL.");
            Console.ResetColor();
            Console.Write("URL: ");
            return Console.ReadLine();
        }

        #endregion Menu/Output Methods

        #region Conversions Methods
        private static double ConvertToMinutes(string interval)
        {
            string[] i = interval.Replace("every", "").Split(' ');
            double frequency = 0.0;
            double.TryParse(i[1], out frequency);

            if (interval.Contains("hour"))
                return (60) * frequency;

            if (interval.Contains("day"))
                return (60 * 24) * frequency;

            if (interval.Contains("week"))
                return (60 * 24 * 7) * frequency;

            return 0;
        }

        private static string ConvertToEmail(List<string> lines)
        {
            string email = lines.First();

            switch (lines.Last().ToLower().Replace("-", ""))
            {
                case "sprint":
                    email += "@messaging.sprintpcs.com";
                    break;

                case "tmobile":
                    email += "@tmomail.net";
                    break;

                case "verizon":
                    email += "@vtext.com";
                    break;

                default:
                    Console.WriteLine("Provider name is invalid, delete everything in number.txt and run program again.");
                    email += "@vtext.com";
                    break;

            }

            return email;
        }
        #endregion Conversions Methods

        #region Check Methods
        private static bool IntervalChecks(string interval)
        {
            string[] split = interval.Split(' ');
            double result = 0;

            if (split.Length != 3) return false;
            bool isNumber = double.TryParse(split[1], out result);

            return
            (
                !string.IsNullOrEmpty(interval)
                && isNumber
                && interval.Contains("every")
                && (interval.Contains("hour") || interval.Contains("day") || interval.Contains("week"))
            );
        }

        private static Search CheckForDuplicates(List<Search> savedSearches, string url)
        {
            foreach (var search in savedSearches)
                if (search.Url == url)
                    return search;

            return null;
        }

        private static bool TimeToCheck(Search search)
        {
            var diff = DateTime.Now - search.LastRunTime;
            double intervalMinutes = ConvertToMinutes(search.Interval.ToLower());

            return diff.TotalMinutes > intervalMinutes;
        }

        #endregion Check Methods

        #region Get Methods
        public static string GetWorkingDir()
        {
            return Environment.CurrentDirectory;
        }

        private static string GetNumber()
        {
            List<string> lines = new List<string>();
            string line = string.Empty;

            System.IO.StreamReader fileReader = new System.IO.StreamReader("number.txt");
            while ((line = fileReader.ReadLine()) != null)
                lines.Add(line);

            fileReader.Close();

            if (lines.Count < 2)
            {
                // Get phone number and validate it
                Console.Write("Enter your phone number: ");
                string number = Console.ReadLine().ToLower().Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "").Trim();
                while (!ValidNumber(number))
                {
                    Console.Write("Invalid!\nEnter your phone number: ");
                    number = Console.ReadLine().ToLower();
                }
                lines.Add(number);

                // Get provider name and validate it
                Console.Write("Enter your phone provider (tmobile, verizon): ");
                string provider = Console.ReadLine().ToLower().Replace("-", "");
                while (!ValidProvider(provider))
                {
                    Console.Write("Invalid!\nEnter your phone provider: ");
                    provider = Console.ReadLine().ToLower();
                }
                lines.Add(provider);

                // Write number and provider to number.txt
                System.IO.StreamWriter fileWriter = new System.IO.StreamWriter("number.txt");
                foreach (var ln in lines)
                    fileWriter.WriteLine(ln);

                fileWriter.Close();
            }

            return ConvertToEmail(lines);
        }

        private static void GetKnownAdIds(Search search, Scraper scraper)
        {
            List<string> adIds = new List<string>();

            foreach (var ad in scraper.KslAds)
                adIds.Add(ad.Data["id"]);

            search.KnownAdIds = adIds;
        }
        #endregion Get Methods

        #region Compare Methods
        private static List<KslAd> CompareAds(List<KslAd> kslAds, List<string> knownAdIds)
        {
            List<KslAd> newAds = new List<KslAd>();

            foreach (var ad in kslAds)
            {
                if (!knownAdIds.Contains(ad.Data["id"]))
                    newAds.Add(ad);
            }

            return newAds;
        }
        #endregion Compare Methods

        #endregion Helper Methods

        #region Validation Methods
        private static void ValidateName(string name)
        {
            bool valid = !string.IsNullOrEmpty(name) && name.Length > 2;

            while (!valid)
            {
                Console.WriteLine("You must enter a name with at least 3 characters");
                Console.WriteLine("Please try again.");
                Console.Write("Name (Dodge Ram 2500): ");

                name = Console.ReadLine();
                valid = !string.IsNullOrEmpty(name) && name.Length > 2;
            }
        }

        private static void ValidateInterval(string interval)
        {
            bool valid = IntervalChecks(interval);

            while (!valid)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nYou must enter a valid interval.");
                Console.WriteLine("Something like, 'every 2 hours' or 'every 8 days' or 'every 1 week'.");
                Console.WriteLine("Please try again.");
                Console.ResetColor();
                Console.Write("Interval: ");

                interval = Console.ReadLine();
                valid = IntervalChecks(interval);
            }
        }

        private static List<KslAd> ValidateUrl(List<Search> savedSearches, string url)
        {
            bool valid = !string.IsNullOrEmpty(url) && url.Contains("ksl.com");
            Search duplicateSearch = CheckForDuplicates(savedSearches, url);

            while (!valid || duplicateSearch != null)
            {
                url = AskForNewUrl(duplicateSearch);
                valid = !string.IsNullOrEmpty(url) && url.Contains("ksl.com");
                duplicateSearch = CheckForDuplicates(savedSearches, url);
            }

            Search search = new Search() { Url = url };
            Scraper scraper = new Scraper(url);

            while (!valid || scraper.KslAds.Count == 0)
            {
                url = AskForNewUrl(duplicateSearch);
                valid = !string.IsNullOrEmpty(url) && url.Contains("ksl.com");
                duplicateSearch = CheckForDuplicates(savedSearches, url);

                if (valid && duplicateSearch == null)
                {
                    search = new Search() { Url = url };
                    scraper = new Scraper(url);
                }
            }

            return scraper.KslAds;
        }

        private static bool ValidProvider(string provider)
        {
            string[] validProviders = new[] { "verizon", "tmobile", "sprint" };

            return validProviders.Contains(provider.ToLower());
        }

        private static bool ValidNumber(string number)
        {
            return number.Length == 10;
        }
        #endregion Validation Methods
    }
}
