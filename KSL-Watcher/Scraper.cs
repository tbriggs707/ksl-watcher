﻿using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSL_Watcher
{
    public class Scraper
    {
        public string Url { get; set; }
        public List<KslAd> KslAds { get; private set; }

        private ScrapingBrowser browser;
        private WebPage pageResult;
        private IEnumerable<HtmlNode> groups;

        public Scraper(string url)
        {
            try
            {
                Url = url;
                KslAds = new List<KslAd>();

                browser = new ScrapingBrowser() { AllowAutoRedirect = true, AllowMetaRedirect = true };
                pageResult = browser.NavigateToPage(new Uri(url));
                groups = pageResult.Html.CssSelect(".listing");

                GetData();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void GetData()
        {
            foreach (var group in groups)
            {
                var ad = ParseHTML(group);
                KslAds.Add(ad);
            }
        }

        private KslAd ParseHTML(HtmlNode group)
        {
            KslAd ad = new KslAd();

            ad.Data["id"] =          getAdId(group);
            ad.Data["url"] =         getAdUrl(group);
            ad.Data["make"] =        getAdMake(group);
            ad.Data["model"] =       getAdModel(group);
            ad.Data["imgUrl"] =      getImgUrl(group);
            ad.Data["title"] =       getAdTitle(group);
            ad.Data["price"] =       getAdPrice(group);
            ad.Data["mileage"] =     getAdMileage(group);
            ad.Data["location"] =    getAdLocation(group);
            ad.Data["description"] = getAdDescription(group);

            return ad;
        }

        #region Helper Methods
        private string getAdId(HtmlNode group)
        {
            try { return group.Attributes[1].Value; }
            catch { return "N/A"; }
        }

        private string getAdUrl(HtmlNode group)
        {
            try { return "http://www.ksl.com" + group.ChildNodes[3].ChildNodes[1].Attributes[1].Value; }
            catch { return "N/A"; }
        }

        private string getAdMake(HtmlNode group)
        {
            try { return group.Attributes[2].Value; }
            catch { return "N/A"; }
        }

        private string getAdModel(HtmlNode group)
        {
            try { return group.Attributes[3].Value; }
            catch { return "N/A"; }
        }

        private string getImgUrl(HtmlNode group)
        {
            try { return group.ChildNodes[1].ChildNodes[1].ChildNodes[1].ChildNodes[1].Attributes[0].Value; }
            catch { return "N/A"; }
        }

        private string getAdTitle(HtmlNode group)
        {
            try { return group.ChildNodes[3].ChildNodes[1].ChildNodes[0].InnerText.Trim(); }
            catch { return "N/A"; }
        }

        private string getAdPrice(HtmlNode group)
        {
            try { return group.ChildNodes[5].InnerHtml.Trim(); }
            catch { return "N/A"; }
        }

        private string getAdMileage(HtmlNode group)
        {
            try { return group.ChildNodes[7].InnerHtml.Trim().Replace("Mileage: ", ""); }
            catch { return "N/A"; }
        }

        private string getAdLocation(HtmlNode group)
        {
            try { return group.ChildNodes[9].ChildNodes[0].InnerText.Replace("|", "").Trim(); }
            catch { return "N/A"; }
        }

        private string getAdDescription(HtmlNode group)
        {
            try { return group.ChildNodes[11].InnerText.Trim(); }
            catch { return "N/A"; }
        }
        #endregion    
    }
}
