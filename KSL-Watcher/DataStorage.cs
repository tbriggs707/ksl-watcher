﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KSL_Watcher
{
    public class DataStorage
    {
        private XmlSerializer xmlSerializer;

        public DataStorage()
        {
            xmlSerializer = new XmlSerializer(typeof(List<Search>));
        }

        public List<Search> ReadFromFile(string filePath)
        {
            StreamReader reader = new StreamReader(filePath);
            List<Search> data = xmlSerializer.Deserialize(reader.BaseStream) as List<Search>;
            reader.Close();
            return data;
        }

        public void WriteResults(string filePath, List<Search> data)
        {
            StreamWriter writer = new StreamWriter(filePath);
            xmlSerializer.Serialize(writer, data);
            writer.Close();
        }
    }
}
