﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSL_Watcher
{
    public class Search
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public List<string> KnownAdIds { get; set; }
        public DateTime LastHit { get; set; }
        public DateTime LastRunTime { get; set; }
        public string Interval { get; set; }
      
        public Search() { }

        public Search(string name, string url)
        {
            Name = name;
            Url = url;
            KnownAdIds = new List<string>();
            LastHit = new DateTime();
            LastRunTime = new DateTime();
            Interval = string.Empty;
        }
    }
}
