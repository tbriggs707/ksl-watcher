﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.IO;

namespace KSL_Watcher
{
    public class SMS
    {
        private SmtpClient smtp;
        private MailAddress fromAddress;
        private MailAddress toAddress;
        private string email;
        private string cred;

        public SMS(string number)
        {
            GetCred();
            fromAddress = new MailAddress(email, "KSL Watcher");
            toAddress = new MailAddress(number, Environment.UserName);

            smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, Crypto.Decrypt(cred)),
                Timeout = 20000
            };
        }

        public void Send(string msg)
        {
            using (var message = new MailMessage(fromAddress, toAddress) { Body = msg })
            {
                if (!toAddress.ToString().Contains("@"))
                {
                    Console.WriteLine("Provider name is invalid, delete everything in\n number.txt and run the program again.");
                    return;
                }

                smtp.Send(message);
                Thread.Sleep(100);
            }
        }

        #region Credential Methods
        private void ResetCred()
        {
            bool validEmail = false;
            string compareEmail = string.Empty;
            string plainCred = Crypto.Decrypt(cred);
            string comparePassword = string.Empty;
            string encryptedPassword = cred;

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Error reading the .cred file's values.");
            Console.WriteLine("This is the email used to send the texts.");
            Console.WriteLine("Please re-enter your email's login info.");
            Console.ResetColor();

            // Email
            while (!validEmail || string.IsNullOrEmpty(compareEmail) || email != compareEmail)
            {
                Console.Write("Email: ");
                email = Console.ReadLine();
                Console.Write("Verify Email: ");
                compareEmail = Console.ReadLine();

                if (!ValidateEmail(email)) InvalidEmail();
                if (email != compareEmail) TryAgain();
            }

            // Credential
            while (string.IsNullOrEmpty(comparePassword) || Crypto.Decrypt(encryptedPassword) != Crypto.Decrypt(comparePassword))
            {
                Console.Write("Email's Password: ");
                encryptedPassword = Crypto.Encrypt(GetPasswordInput());
                Console.WriteLine();

                Console.Write("Verify Password: ");
                comparePassword = Crypto.Encrypt(GetPasswordInput());

                if (Crypto.Decrypt(encryptedPassword) != Crypto.Decrypt(comparePassword))
                    TryAgain();
                else
                    cred = encryptedPassword;
            }

            SaveCredToFile();
            ReadCredFromFile();
        }

        private void GetCred()
        {
            List<string> lines = ReadCredFromFile();

            if (lines.Count != 2)
                ResetCred();
            else
            {
                email = lines[0];
                cred = lines[1];
            }
        }

        private List<string> ReadCredFromFile()
        {
            if (!File.Exists(".cred"))
                File.Create(".cred").Close();

            List<string> lines = new List<string>();
            string line = string.Empty;

            StreamReader fileReader = new StreamReader(".cred");
            while ((line = fileReader.ReadLine()) != null)
                lines.Add(line);

            fileReader.Close();

            return lines;
        }

        private void SaveCredToFile()
        {
            StreamWriter fileWriter = new StreamWriter(".cred");
            fileWriter.WriteLine(email);
            fileWriter.WriteLine(cred);
            fileWriter.Close();
        }

        private bool ValidateEmail(string email)
        {
            return email.Contains("@");
        }

        // https://msdn.microsoft.com/en-us/library/471w8d85(v=vs.110).aspx
        private string GetPasswordInput()
        {
            string input = string.Empty;
            ConsoleKeyInfo key;

            do
            {
                key = Console.ReadKey(true);
                input += key.KeyChar;
            }
            while (key.Key != ConsoleKey.Enter);

            return input;
        }

        private void InvalidEmail()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nThe email is invalid and does not contain a '@', please try again.");
            Console.ResetColor();
        }

        private void TryAgain()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nThe values entered do not match, please try again.");
            Console.ResetColor();
        }

        #endregion Credential Methods
    }
}
