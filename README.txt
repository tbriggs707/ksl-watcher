This app will take a KSL search URL, ask how often to check for 
new results and then the app will send a text message to the user
when a new result is found.

This works for all KSL classifieds but was created to help buy
cars since the best deals are sold within minutes.

ksl-watcher works best in conjuction with ksl-service so that
it can run in the background automatically on a computer.